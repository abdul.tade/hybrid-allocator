# include <iostream
# include <tuple>
# include <map>
# include <chrono>


template <typename T>
struct is_non_void {
    using type = T;
    static constexpr bool value = true;
};


template<>
struct is_non_void<void> {
    static constexpr bool value = false;
};

template <typename K, typename V>
bool operator==(const std::pair<K,V> &rhs, const std::pair<K,V> &lhs) {
    return (rhs.first == lhs.first) &&
            (rhs.second == lhs.second);
}


template <typename K, typename V>
bool operator==(const std::pair<const K,V> &rhs, const std::pair<K,V> &lhs) {
    return (rhs.first == lhs.first) && 
            (rhs.second == lhs.second);
}


template <typename K, typename V>
bool operator==(const std::pair<K,V> &rhs, const std::pair<const K,V> &lhs) {
    return (rhs.first == lhs.first) &&
            (rhs.second == lhs.second);
}

template <typename Function, typename DurationType, uint64_t threshHoldDuration>
struct Cache {


    static const int64_t MAX_REF_COUNT = 0xF;
    static bool isPointerType;


    template <typename T>
    struct Data {
        T data;
        std::chrono::system_clock::time_point lastTimeAccessed;

        bool operator==(const Data<T> &p) const {
            return this->data == p.data;
        }

        bool operator!=(const Data<T> &p) const {
            return not operator==(p);
        }

 };


    Cache(Function func)
        : _func(func)
    {
        static_assert((threshHoldDuration != 0) , "threshHoldDuration must not be zero");
    }


    /**
        Instead of decrementing for every operation we decrement or increment at regular intervals,
        By this way we don't get imposed the heavy penalty of looping through the map every time, when a value is accessed or need to be updated.
        to decrement or increment the reference count.
        We also dont incur the problem of 
     */
    template <typename... Args>
    auto operator()(Args&&... args) {
        using R = decltype(_func(std::forward<Args>(args) ...));


        static_assert(is_non_void<R>::value, "return type of function cannot be void"); 
        using ArgData = Data<R>;


        std::tuple<Args...> arguments = std::tie(args...);
        static std::map<std::tuple<Args...>, ArgData> cacheMap;


        auto iter = cacheMap.find(arguments);
        --intervalCheckCount;


        auto freeCacheItem = [&](const std::pair<std::tuple<Args...>, ArgData> &p) mutable {
            for (auto& cachePair : cacheMap) {
                if (cachePair == p) {
                                        continue;
                }
                else {
                    auto durationCount = timeSeparation(cachePair.second.lastTimeAccessed);
                    if (durationCount >= threshHoldDuration) {
                        cacheMap.erase(cachePair.first);
                    }
                }
            }
        };

if (iter != cacheMap.end()) { 
            iter->second.lastTimeAccessed = now();
            if (intervalCheckCount <= 0) {
                auto pairToFree = std::make_pair(iter->first, iter->second);
                freeCacheItem(pairToFree);
                intervalCheckCount = MAX_REF_COUNT;
            }
            return cacheMap.at(arguments).data;
        }
        else 
        {
            R result = _func(std::forward<Args>(args) ...);
            ArgData data;
            data.data = result;
            data.lastTimeAccessed = std::chrono::system_clock::now();


            auto Pair = std::pair<std::tuple<Args...>, ArgData>{arguments, data};
            cacheMap.insert(Pair);

            if (intervalCheckCount <= 0) {
                freeCacheItem(Pair);
                intervalCheckCount = MAX_REF_COUNT;
            }
            return result;
        }
    }


private:
    typename std::conditional<std::is_function<Function>::value,
                              typename std::add_pointer<Function>::type,
                              Function>::type _func;
    int64_t intervalCheckCount{MAX_REF_COUNT};


    auto now() {
        return std::chrono::system_clock::now();
    }


    uint64_t timeSeparation(const std::chrono::system_clock::time_point &p) {
        auto nowDuration = now() - p;
        return std::chrono::duration_cast<DurationType>(nowDuration).count();
    }
};

int main()
{
    auto add = [](int x, int y) {
        return x + y;
    };


    Cache<decltype(add),std::chrono::milliseconds,1000> cache(add);
    auto&& res = cache(10, 10);


    std::cout << res << std::endl;
}
