# include <iostream>
# include <map>
# include <tuple>

//# define DEBUG_PRINT_ENABLE

template <typename ObjectType, std::size_t POOL_SIZE>
class HybridAllocator
{
public:

    using value_type = ObjectType;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using pointer = ObjectType*;
    using const_pointer = const ObjectType*;
    using reference = ObjectType&;
    using const_reference = const ObjectType&;

    HybridAllocator()
    {
        ListNode* ptr = static_cast<ListNode*>(std::malloc(sizeof(ListNode)));
        if (ptr == nullptr) {
            throw std::bad_alloc{};
        }
        _poolHead = ptr;
        _poolEnd = _poolHead;
    }

    virtual ~HybridAllocator() {
        ListNode* head = _poolHead;
        while (head != nullptr) 
        {
            ListNode* next = head->next;
            std::free(head);
            head = next;
        }
    }

    ObjectType* allocate() {

        auto [ objectIndex, list_node ] = findFree();

        if (list_node == nullptr) {
            ListNode* node_ptr = static_cast<ListNode*>(std::malloc(sizeof(ListNode)));
            if (node_ptr == nullptr) {
                throw std::bad_alloc{};
            }
            node_ptr->next = nullptr;
            _poolEnd->next = node_ptr;
            node_ptr->nodes[0].is_allocated = true;
            _poolEnd = node_ptr;
            node_ptr->useCount = 1;
            return &node_ptr->nodes[0].object;
        }

        list_node->useCount += 1;
        return &list_node->nodes[objectIndex].object;
    }

    void deallocate(ObjectType* object) 
    {
        auto [ objectNodeIndex, prev_list_node, list_node ] = findObjectPtr(object); // structurable tuple binding
        if (list_node == nullptr) {
            throw std::bad_alloc{};
        }

        else if (list_node->useCount != 0) {
            list_node->nodes[objectNodeIndex].is_allocated = false;
            list_node->useCount -= 1;
        }

        else if (list_node->useCount == 0){
            prev_list_node->next = list_node->next;
            std::free(list_node);
        }
    }

private:

    struct __attribute__((packed)) ObjectNode {
        bool is_allocated{};
        ObjectType object;
    };

    struct __attribute__((packed)) ListNode {
        ObjectNode nodes[POOL_SIZE]; //pool of object nodes
        ListNode* next{}; // next list node in the list
        uint64_t useCount; /*to decide whether to release pool of objects back to the OS*/
    };

     std::tuple<std::size_t, ListNode*> findFree() 
    {
        ListNode* list_node = _poolHead;
        while (list_node != nullptr)
        {
            for (std::size_t i = 0; i < POOL_SIZE; i++) 
            {
                if (!list_node->nodes[i].is_allocated) {
                    list_node->nodes[i].is_allocated = true;
                    return {i, list_node};
                }
            }

            list_node = list_node->next;
        }
        return {0, nullptr};
    }

    std::tuple<std::size_t, ListNode*, ListNode*> findObjectPtr(ObjectType* ptr) 
    {
        ListNode* list_node = _poolHead;
        ListNode* prev_list_node = nullptr;
        while (list_node != nullptr)
        {
            for (std::size_t i = 0; i < POOL_SIZE; i++) 
            {
                if ((&list_node->nodes[i].object) == ptr and list_node->nodes[i].is_allocated) {
                    return {i, prev_list_node, list_node};
                }
            }
            prev_list_node = list_node;
            list_node = list_node->next;
        }
        return {0, nullptr, nullptr};
    }

    ListNode* _poolHead{};
    ListNode* _poolEnd{};
};

int main()
{
    HybridAllocator<int, 2> allocator{};

    allocator.allocate();
    allocator.allocate();

    allocator.allocate();
    allocator.allocate();

    allocator.allocate();
    allocator.allocate();

    allocator.allocate();
    allocator.allocate();
}