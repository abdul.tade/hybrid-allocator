# Hybrid Allocator

#### What is an Allocator
<p>Dynamic memory allocation is when application request memory at runtime from the operating system. These requires system calls to operating system. These system calls are very expensive. System calls have to setup a predefined set of registers defined by the specific operating system standard and populate this registers with parameters that identify the type of system call and any paramters it needs. These repeated switching back and forth from the <i>kernel space</i> to <i>userspace</i> to allocate memory is very slow.</p>
<p> Allocators have the capability to manage and free allocated memory from the operating system. So when memory is freed the allocator determines if it should be released back to the operating system or it should just be marked as free. That way we dont incur the extra penalty to free that memory that might be reused later.</p>

There are many different type of allocators namely:
* Free list allocators
* Pool allocators

#### Free list allocator
Free list use the the nodes of a linkedlist to store allocated objects. 

* Advantages
  - very flexible because there is no constraint to make the allocated memory contagious

* Disadvantages
  - Due to the allocated memory not being contiguous, there is a lot of cache misses which heavily degrades performance

#### Pool allocators
Pool allocators are allocators that allocate a large block of fixed memory block once and allocate and deallocate memory from these pool.

* Advantages
  - Fixed size and contiguous so therefore boost in performance due to cache hits. Allocator is much faster than free list allocator.

* Disadvantages
  - Due to it being fixed size, its not very suitable for allocating memory of variable sizes or growable memory.

#### Hybrid Allocator
Hybrid Allocator attempts to bring the best of both worlds. It allocates fixed sized pools and each pool is linked to another pool by a linked list. That way we get performance boost from accessing contiguous memory (cache hits in memory access) and also dont restrict ourselves when the pool memory gets exhausted (at least in theory). This is a preliminary solution. More improvements are possible.

##### Improvements
* useCount
  - for each node of the linkedlist which is a pool, we add a field useCount that tracks whether the pool has been completely freed or has some allocated memory resident in it. For every deallocation operation the useCount is decremented by one and for every allocation it is incremented by one. This happens till the useCount wounds up being zero. In this case std::free is invoked which returns memory back to the operating system. This was added to overcome the issue where after sometime our allocator memory grows, for a long running application this can consume significant amount of memory.
