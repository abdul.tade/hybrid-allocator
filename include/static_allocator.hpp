#include <iostream>
#include <cstring>

template <typename T>
struct StaticAllocator
{

    StaticAllocator()
    {
#if defined MAX_ALLOCATOR_SIZE
        static ObjectNode arr[MAX_ALLOCATOR_SIZE];
        size_ = MAX_ALLOCATOR_SIZE;
#else
        static ObjectNode arr[4096] = {0};
        size_ = 4096;
#endif
        std::memset(arr, 0x0, sizeof(arr));
        data_ = arr;
    }

    template <class... Args>
    T *operator()(Args &&...args)
    {
        T *object = allocate();
        *object = T{args...};
        return object;
    }

    T *allocate()
    {
        auto head = data_;

        for (size_t i = 0; i < size_; i++)
        {
            ObjectNode *node = data_ + i;
            if (not node->is_allocated)
            {
                node->is_allocated = true;
                return &node->data;
            }
        }

        throw std::runtime_error{"static memory allocator full"};
    }

    void deallocate(T *ptr)
    {
        ObjectNode *head = data_;

        for (size_t i = 0; i < size_; i++)
        {
            ObjectNode *node = head + i;
            if (&node->data == ptr && node->is_allocated)
            {
                node->is_allocated = false;
                return;
            }
        }

        throw std::runtime_error{"Cannot deallocate memory, does not belong memory pool"};
    }

private:
    struct __attribute__((packed)) ObjectNode
    {
        bool is_allocated{false};
        T data;
    };

    ObjectNode *data_;
    size_t size_;
};
